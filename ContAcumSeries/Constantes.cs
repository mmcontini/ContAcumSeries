﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CancionesApp
{
    /// <summary>Clase Carga de constantes de la aplicación</summary>
    static class Constantes
    {
        // Estructura switch
        public const String UNO = "1";
        public const String DOS = "2";
        public const String TRES = "3";
        public const String CUATRO = "4";
        public const String CINCO = "5";

    }
}
