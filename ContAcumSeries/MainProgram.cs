﻿using CancionesApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.IO;

/// <summary>Clase Core de la app - Procesamiento lógico</summary>
class MainProgram
{

    static String opcionvolver = null;

    /// <summary>Método de ejecución</summary>
    /// <param name="args">Array con parámetros</param>  
    static void Main(string[] args)
    {
        ImprimirPantallaMensajesMenu();
        String opcionElegida = Console.ReadLine();
        ProcesarOpciones(opcionElegida);
    }

    /// <summary>Método Lógica de procesamiento de opciones elegidas</summary>
    /// <param name="opcion">Opción elegida</param>
    static void ProcesarOpciones(String opcion)
    {
        switch (opcion)
        {
            case Constantes.UNO:
                ProcesarContador();
                break;
            case Constantes.DOS:
                ProcesarAcumulador();
                break;
            case Constantes.TRES:
                ProcesarFibonacci();
                break;
            case Constantes.CUATRO:
                Console.WriteLine("Hallado en índice: {0}", BusquedaFibonacci());
                VolverAlMenu();
                break;
            case Constantes.CINCO:
                Console.WriteLine(" \r\n ");
                Console.WriteLine(" ********************* Fin de la ejecución ********************* ");
                break;
            default:
                break;
        }

    }

    /// <summary>Método Lógica para crear un contador</summary>
    static void ProcesarContador()
    {
        Console.Clear();
        Console.WriteLine("Ingresa cantidad de Equipos asignados: ");
        int n;
        String cantidad = Console.ReadLine();
        bool isNumeric = int.TryParse(cantidad, out n);

        if (isNumeric)
        {
            int desktop, notebook, k, tipo;
            double porcentajeD, porcentajeN;
            desktop = 0;
            notebook = 0;
            for (k = 1; k <= n; k++)
            {
                Console.Clear();
                Console.WriteLine("Equipo asignado: " + k);
                Console.WriteLine("Tipos de Equipos");
                Console.WriteLine("1. Desktop");
                Console.WriteLine("2. Notebook");
                //TODO - 1: Implementar el tipo Tablet
                Console.Write("Ingresa tipo: ");
                tipo = int.Parse(Console.ReadLine());
                if (tipo == 1)
                    desktop = desktop + 1;
                if (tipo == 2)
                    notebook = notebook + 1;
            }
            porcentajeD = desktop * 100.0 / (desktop + notebook);
            porcentajeN = notebook * 100.0 / (desktop + notebook);
            Console.WriteLine("");
            Console.WriteLine("*********** Resultados ************");
            Console.WriteLine("Cantidad de Desktop es: " + desktop + " porcentaje es: " + porcentajeD);
            Console.WriteLine("Cantidad de notebook es: " + notebook + " porcentaje es: " + porcentajeN);
            Console.ReadLine();

        }
        else
        {
            Console.WriteLine(" Error: Debe ingresar algún valor ");
        }
        VolverAlMenu();
    }

    /// <summary>Método Lógica para procesar un acumulador</summary>
    static void ProcesarAcumulador()
    {
        Console.Clear();
        Console.WriteLine(" ********************* Procesamiento de acumulador ********************* ");
        int n, k;
        double numero, total;
        total = 0;
        Console.WriteLine("Suma de N números");
        Console.Write("Ingresa N: ");
        n = int.Parse(Console.ReadLine());
        Console.WriteLine(" Ingreso de datos ");
        for (k = 1; k <= n; k++)
        {
            Console.Write(">>>>> Ingresa el numero: ");
            numero = double.Parse(Console.ReadLine());
            //TODO - 2: Implementar funcionalidad acumulador.
        }
        Console.WriteLine(" >>>>> Resultado final <<<<< ");
        Console.WriteLine("La suma de los números es: " + total);
        Console.ReadLine();
        VolverAlMenu();
    }

    /// <summary>Método Lógica para la serie de Fibonacci</summary>
    static void ProcesarFibonacci()
    {
        Console.Clear();
        int i, conteo, f0 = 0, f1 = 1, f2 = 0;
        Console.Write("Ingresar el límite : ");
        conteo = int.Parse(Console.ReadLine());
        Console.WriteLine(f0);
        Console.WriteLine(f1);
        for (i = 0; i <= conteo; i++)
        {
            f2 = f0 + f1;
            Console.WriteLine(f2);
            //TODO - 3: Complementar lógica de Fibonacci para producir la serie.
        }
        Console.ReadLine();
        VolverAlMenu();
    }

    // Función de utilidad para hallar el mínimo de 2 elementos
    static int min(int x, int y) { return (x <= y) ? x : y; }


    /// <summary>Método Lógica para busqueda Fibonacci</summary>
    static int BusquedaFibonacci()
    {
        Console.Clear();
        int[] arr = { 11, 21, 36, 42, 49, 51, 81, 82, 85, 93, 103 };

        Console.Write("Ingresar número a buscar en array: ");
        int e = int.Parse(Console.ReadLine());

        /* Inicializa números de Fibonacci */
        int fibMMm2 = 0;  // Número de Fibonacci (m-2)-ésimo.
        int fibMMm1 = 1;  // Número de Fibonacci (m-1)-ésimo.
        int fibM = fibMMm2 + fibMMm1;  // Número de Fibonacci m-ésimo 

        /* fibM va a almacenar el número de Fibonacci más pequeño mayor ó igual a longitud del array. */
        while (fibM < arr.Length)
        {
            fibMMm2 = fibMMm1;
            fibMMm1 = fibM;
            fibM = fibMMm2 + fibMMm1;
        }

        // Marca el rango eliminado del frente
        int offset = -1;

        /* Mientras hay elementos a ser inspeccionados. Notar que 
          comparamos arr[fibMm2] con e. Cuando fibM se vuelve 1, fibMm2 se vuelve 0 */
        while (fibM > 1)
        {
            // Chequear si fibMm2 es una ubicación válida
            int i = min(offset + fibMMm2, arr.Length - 1);

            /* Si e es mayor que el valor en el índice fibMm2, se corta el array subarray del offset a i */
            if (arr[i] < e)
            {
                fibM = fibMMm1;
                fibMMm1 = fibMMm2;
                fibMMm2 = fibM - fibMMm1;
                offset = i;
            }
            /* Si e es menor que el valor en el índice fibMm2, se corta el Subarray después de i+1 */
            else if (arr[i] > e)
            {
                fibM = fibMMm2;
                fibMMm1 = fibMMm1 - fibMMm2;
                fibMMm2 = fibM - fibMMm1;
            }
            /* Se ha hallado elemento. Se retorna el índice. */
            else return i;
        }

        /* Se compara el último elemento con e. */
        if (fibMMm1 == 0 && arr[offset + 1] == e) return offset + 1;

        /* Elemento no encontrado. Se retorna -1 */
        return -1;

    }

    /// <summary>Método Vuelta al menú</summary>
    static void VolverAlMenu()
    {
        Console.WriteLine(" \r\n ");
        Console.WriteLine(" Presione cualquier tecla para volver al menú ..... ");
        opcionvolver = Console.ReadLine();
        Console.Clear();
        Main(new String[0]);
    }

    /// <summary>Método Impresión mensajes estáticos menú</summary>
    static void ImprimirPantallaMensajesMenu()
    {
        Console.WriteLine(" ********************************************************** ");
        Console.WriteLine(" Ingrese a una de las siguientes opciones: ");
        Console.WriteLine(" 1: Contador ");
        Console.WriteLine(" 2: Acumulador ");
        Console.WriteLine(" 3: Serie Fibonacci ");
        Console.WriteLine(" 4: Búsqueda Fibonacci ");
        Console.WriteLine(" 5: Salir ");
        Console.WriteLine(" ********************************************************** ");
    }
}